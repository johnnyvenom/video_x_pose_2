import { writable } from 'svelte/store';

// Create a writeable store to hold the selected video
// This is set in the RightSidebar component
// and read by MainDisplay, which loads the proper video
export const selectedVideo = writable(null);

// Store to hold the video element
// This is the MainDisplay <video> element 
// which is controlled in the Controls component 
export const videoElement = writable(null);