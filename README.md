video_x_pose 2
==============

This is the second version of an experimental web application for creating dance movements with simulated 3D visualizations from single camera videos. It uses [Google MediaPipe](https://developers.google.com/mediapipe/solutions/vision/pose_landmarker) for human pose estimation and is built using [SvelteKit](https://kit.svelte.dev). 

This project is conducted by researcher John Sullivan and is part of the "Living Archive: Interactive Documentation of Dance" research project at the *[Laboratoire Interdisciplinaire des Sciences Numerique](https://www.lisn.upsaclay.fr/)*, Université Paris-Saclay, Paris, France.

The first limited version of the app can be found at https://johnnyvenom.gitlab.io/video-x-pose/. 

For more information, please contact [John Sullivan](https://gitlab.com/johnnyvenom). 


---
---

# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/main/packages/create-svelte).

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm create svelte@latest

# create a new project in my-app
npm create svelte@latest my-app
```

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
